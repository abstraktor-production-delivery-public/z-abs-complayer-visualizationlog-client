
'use strict';


class LogFilter {
  constructor() {
    //this.eventListeners = new Map();
    this.type = [false, false, true, true, true, true, true, true, false, true, true];
  }

  init(log) {
    this.type[LogFilter.Engine] = log.filterEngine;
    this.type[LogFilter.Debug] = log.filterDebug;
    this.type[LogFilter.Error] = log.filterError;
    this.type[LogFilter.Warning] = log.filterWarning;
    this.type[LogFilter.IP] = log.filterIP;
    this.type[LogFilter.GUI] = log.filterGUI;
    this.type[LogFilter.Success] = log.filterSuccess;
    this.type[LogFilter.Failure] = log.filterFailure;
    this.type[LogFilter.TestData] = log.filterTestData;
    this.type[LogFilter.BrowserLog] = log.filterBrowserLog;
    this.type[LogFilter.BrowserErr] = log.filterBrowserErr;
  }
  
  update(log) {
    if(this.type[LogFilter.Error] !== log.filterError) {
      this.setFilterError(log.filterError);
    }
    if(this.type[LogFilter.Warning] !== log.filterWarning) {
      this.setFilterWarning(log.filterWarning);
    }
    if(this.type[LogFilter.Success] !== log.filterSuccess) {
      this.setFilterSuccess(log.filterSuccess);
    }
    if(this.type[LogFilter.Failure] !== log.filterFailure) {
      this.setFilterFailure(log.filterFailure);
    }
    if(this.type[LogFilter.Engine] !== log.filterEngine) {
      this.setFilterEngine(log.filterEngine);
    }
    if(this.type[LogFilter.TestData] !== log.filterTestData) {
      this.setFilterTestData(log.filterTestData);
    }
    if(this.type[LogFilter.Debug] !== log.filterDebug) {
      this.setFilterDebug(log.filterDebug);
    }
    if(this.type[LogFilter.IP] !== log.filterIP) {
      this.setFilterIP(log.filterIP);
    }
    if(this.type[LogFilter.GUI] !== log.filterGUI) {
      this.setFilterGUI(log.filterGUI);
    }
    if(this.type[LogFilter.BrowserLog] !== log.filterBrowserLog) {
      this.setFilterBrowserLog(log.filterBrowserLog);
    }
    if(this.type[LogFilter.BrowserErr] !== log.filterBrowserErr) {
      this.setFilterBrowserErr(log.filterBrowserErr);
    }
  }
  
  /*addEventListeners(events, eventListener) {
    events.eventNames.forEach((eventName) => {
      if(!this.eventListeners.has(eventName)) {
        this.eventListeners.set(eventName, []);
      }
      const eventListeners = this.eventListeners.get(eventName);
      eventListeners.push(eventListener);
      events.pIds.push(eventListeners.length - 1);
    });
  }
  
  removeEventListeners(events, pIds) {
    let index = 0;
    events.eventNames.forEach((eventName) => {
      const eventListeners = this.eventListeners.get(eventName);
      if(events.pIds[index] !== eventListeners.length) {
        const last = eventListeners[eventListeners.length - 1];
        eventListeners[events.pIds[index]] = last;
      }
      eventListeners.pop();
    });
  }*/
  
  setFilterError(filterError) {
    this.type[LogFilter.Error] = filterError;
    //this._emittEvent(LogFilter.Error);
  }
  
  setFilterWarning(filterWarning) {
    this.type[LogFilter.Warning] = filterWarning;
    //this._emittEvent(LogFilter.Warning);
  }
  
  setFilterSuccess(filterSuccess) {
    this.type[LogFilter.Success] = filterSuccess;
    //this._emittEvent(LogFilter.Success);
  }
  
  setFilterFailure(filterFailure) {
    this.type[LogFilter.Failure] = filterFailure;
    //this._emittEvent(LogFilter.Failure);
  }
  
  setFilterEngine(filterEngine) {
    this.type[LogFilter.Engine] = filterEngine;
   // this._emittEvent(LogFilter.Engine);
  }
  
  setFilterTestData(filterTestData) {
    this.type[LogFilter.TestData] = filterTestData;
    //this._emittEvent(LogFilter.TestData);
  }
  
  setFilterDebug(filterDebug) {
    this.type[LogFilter.Debug] = filterDebug;
    //this._emittEvent(LogFilter.Debug);
  }
  
  setFilterIP(filterIP) {
    this.type[LogFilter.IP] = filterIP;
    //this._emittEvent(LogFilter.IP);
  }
  
  setFilterGUI(filterGUI) {
    this.type[LogFilter.GUI] = filterGUI;
    //this._emittEvent(LogFilter.GUI);
  }
  
  setFilterBrowserLog(filterBrowserLog) {
    this.type[LogFilter.BrowserLog] = filterBrowserLog;
    //this._emittEvent(LogFilter.BrowserLog);
  }
  
  setFilterBrowserErr(filterBrowserErr) {
    this.type[LogFilter.BrowserErr] = filterBrowserErr;
    //this._emittEvent(LogFilter.BrowserErr);
  }
  
  /*_emittEvent(name) {
    const eventListeners = this.eventListeners.get(name);
    if(eventListeners) {
      for(let i = 0; i < eventListeners.length; ++i) {
        eventListeners[i].calculateHeight();
      }
    }
  }*/
}


LogFilter.Engine = 0;
LogFilter.Debug = 1;
LogFilter.Error = 2;
LogFilter.Warning = 3;
LogFilter.IP = 4;
LogFilter.GUI = 5;
LogFilter.Success = 6;
LogFilter.Failure = 7;
LogFilter.TestData = 8;
LogFilter.BrowserLog = 9;
LogFilter.BrowserErr = 10;


module.exports = LogFilter;
