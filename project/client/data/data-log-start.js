
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import HighResolutionDate from 'z-abs-corelayer-cs/clientServer/time/high-resolution-date';


class DataLogStart {
  static size = Encoder.Uint32Size + Encoder.Uint16Size + Encoder.Uint16Size + Encoder.CtSize + Encoder.Uint64Size;
  
  static store(msg, testCaseIteration, testSuiteIteration, guid) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataLogStart.size);
    encoder.setUint32(msg.index + 1);
    encoder.setUint16(testSuiteIteration);
    encoder.setUint16(testCaseIteration);
    encoder.setCtStringInternal(msg.name);
    encoder.setBigUint64(msg.timestamp);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const testCaseId = decoder.getUint32();
    const testSuiteIteration = decoder.getUint16();
    const testCaseIteration = decoder.getUint16();
    const name = decoder.getCtString();
    const date = new HighResolutionDate(decoder.getBigUint64()).getDateMilliSeconds();
    return {
      testCaseId,
      testSuiteIteration,
      testCaseIteration,
      name,
      date
    };
  }
}


module.exports = DataLogStart;
