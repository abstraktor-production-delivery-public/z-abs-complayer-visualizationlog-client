
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import HighResolutionDuration from 'z-abs-corelayer-cs/clientServer/time/high-resolution-duration';


class DataLogEnd {
  static size = Encoder.Uint32Size + Encoder.Uint8Size + Encoder.Uint64Size;
  
  static store(msg) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataLogEnd.size);
    encoder.setUint32(msg.index + 1);
    encoder.setUint8(msg.resultId);
    encoder.setBigUint64(msg.duration);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const testCaseId = decoder.getUint32();
    const resultId = decoder.getUint8();
    const duration = new HighResolutionDuration(decoder.getBigUint64()).getDuration();
    return {
      testCaseId,
      resultId,
      duration
    };
  }
}


module.exports = DataLogEnd;
