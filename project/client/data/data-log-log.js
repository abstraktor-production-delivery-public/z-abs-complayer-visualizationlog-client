
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import HighResolutionDate from 'z-abs-corelayer-cs/clientServer/time/high-resolution-date';
import LogSourceType from 'z-abs-funclayer-engine-cs/clientServer/log/log-source-type';


class DataLogLog {
  static size = Encoder.Uint32Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint64Size + Encoder.CtSize + Encoder.CtSize + Encoder.CtSize + Encoder.Uint8Size + Encoder.Uint32Size;
  
  static store(msg, logId) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const logLength = encoder.getStringBytes(msg.log);
    const buffer = encoder.createBuffer(DataLogLog.size + logLength);
    encoder.setUint32(logId);
    encoder.setUint8(msg.type);
    encoder.setUint8(msg.data ? msg.data.id : 0);
    encoder.setBigUint64(msg.date);
    encoder.setCtStringInternal(msg.actor);
    encoder.setCtStringInternal(msg.actorPath);
    encoder.setCtStringInternal(msg.fileName);
    encoder.setUint8(msg.sourceType);
    encoder.setUint32(msg.lineNumber);
    encoder.setString(msg.log);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const logId = decoder.getUint32();
    const type = decoder.getUint8();
    const dataId = decoder.getUint8();
    const timestamp = decoder.getBigUint64();
    const actor = decoder.getCtString();
    const actorPath = decoder.getCtString();
    const fileName = decoder.getCtString();
    const sourceType = decoder.getUint8();
    const lineNumber = decoder.getUint32();
    const log = decoder.getString();
    
    const date = new HighResolutionDate(timestamp).getDateMilliSeconds();
    let codePath = null;
    if(LogSourceType.SERVER === sourceType) {
      codePath = `/code-editor/project/${fileName}?line=${lineNumber}&type=${type}`;
    }
    else if(LogSourceType.ACTOR === sourceType) {
      codePath = `/actor-editor/${fileName}?line=${lineNumber}&type=${type}`;
    }
    else if(LogSourceType.STACK === sourceType) {
      codePath = `/stack-editor/${fileName}?line=${lineNumber}&type=${type}`;
    }
    return {
      logId: logId,
      type: type,
      dataId: dataId,
      date: date,
      actor: actor,
      actorPath: `/actor-editor/${actorPath}`,
      fileName: `${fileName}:${lineNumber}`,
      codePath: codePath,
      log: log
    };
  }
}


module.exports = DataLogLog;
