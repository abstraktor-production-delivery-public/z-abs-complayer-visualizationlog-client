
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonLogFilter extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue)
      || !this.shallowCompare(this.props.name, nextProps.name)
      || !this.shallowCompare(this.props.colorMark, nextProps.colorMark);
  }
  
  render() {
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={[['glyphicon-check', 'glyphicon-unchecked']]} colorMark={this.props.colorMark} styles={[null]} toolTipContent={this.props.name} toolTipHeading="Filter"
        onAction={this.props.onAction}
      />
    );
  }
}
