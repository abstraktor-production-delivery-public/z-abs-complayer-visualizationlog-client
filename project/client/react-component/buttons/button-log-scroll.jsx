
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonLogScroll extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue);
  }
  
  render() {
    const onLoad = this.props.onLoad ? {onLoad: this.props.onLoad} : {};
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={['glyphicon-sort-by-attributes']} colorMark="connection_events" styles={[null]} toolTipContent={['On', 'Off']} toolTipHeading="Auto scroll"
        onAction={this.props.onAction}
        {...onLoad}
      />
    );
  }
}
