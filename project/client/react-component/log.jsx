
'use strict';

import TemplateLogStart  from '../templates/template-log-start';
import TemplateLogEnd  from '../templates/template-log-end';
import TemplateLogLog  from '../templates/template-log-log';
import ScrollList from 'z-abs-complayer-bootstrap-client/client/scroll-list';
import ScrollListNode from 'z-abs-complayer-bootstrap-client/client/scroll-list-node';
import ReactComponentRealtimeRenderer from 'z-abs-corelayer-client/client/react-component/react-component-realtime-renderer';
import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import React from 'react';


export default class Log extends ReactComponentRealtimeRenderer {
  static INDEX_START = 0;
  static INDEX_LOG = 1;
  static INDEX_END = 2;
  
  constructor(props) {
    super(props, [props.store], 2);
    this.isActive = false;
    this.logId = 0;
    this.name = '';
    this.logClient = false;
    this.logConsole = false;
    this.logClientSettingLog = false;
    this.logClientSettingTc = false;
    this.logClientSettingTs = false;
    this.logClientSettingSum = false;
    this.logClientSettingErr = false;
    this.logConsoleSettingLog = false;
    this.logConsoleSettingTc = false;
    this.logConsoleSettingTs = false;
    this.logConsoleSettingSum = false;
    this.logConsoleSettingErr = false;
    this.refDivHeading = React.createRef();
    this.refDivLog = React.createRef();
    this.logStartTemplate = new TemplateLogStart();
    this.logEndTemplate = new TemplateLogEnd();
    this.logLogTemplate = new TemplateLogLog();
    this.scrollList = new ScrollList(props.name, (autoScroll) => {
      if(props.onAutoScroll) {
        props.onAutoScroll(autoScroll);
      }
    }, (data, cb, ...params) => {
      this.renderRealtimeData(data, cb, ...params);
    }, (left) => {
      this.refDivHeading.current.setAttribute('style', `left:${-left}px;`);
    });
    this.cbIsRealtime = () => {
      return true;
    };
    this.templates = [
      new TemplateLogStart(),
      new TemplateLogLog(),
      new TemplateLogEnd()
    ];
  }
  
  didMount() {
    const storeState = Reflect.get(this.state, this.props.storeName);
    this.props.filter.update(storeState.buttons.log);
    this.initComponents(this.props, false);
    this.scrollList.init(this.refDivLog.current, storeState.buttons.log.scroll, storeState.buttons.log.zoom);
  }
  
  willUnmount() {
    this.scrollList.exit();
  }
  
  realtimeUpdate(nextProps, nextState) {
    const storeState = Reflect.get(this.state, this.props.storeName);
    const nextStoreState = Reflect.get(nextState, nextProps.storeName);
    if(this.props.buttonsLoaded !== nextProps.buttonsLoaded) {
      this.initComponents(nextProps, true);
    }
    if(nextStoreState.buttons.log !== storeState.buttons.log) {
      nextProps.filter.update(nextStoreState.buttons.log);
      this.scrollList.update(nextStoreState.buttons.log.scroll, nextStoreState.buttons.log.zoom, storeState.buttons.log !== nextStoreState.buttons.log);
    }
  }
  
  active(active) {
    const prevActive = this.isActive;
    this.isActive = active;
    this.scrollList.active(active);
    if(!prevActive && active) {
      this.renderRealtimeData();
    }
  }
  
  onRenderRealtimeFrame(timestamp, clear, force) {
    this.scrollList.renderRealtimeFrame(timestamp, clear, force);
  }
  
  _template(index, ...params) {
    const template = this.templates[index];
    if(template) {
      const buffer = template.store(...params);
      const node = new ScrollListNode(template, buffer);
      this.scrollList.add(node);
      this._renderActiveRealtime();
    }
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.LOG === msg.msgId) {
      if(this.logClientSettingLog) {
        this._template(Log.INDEX_LOG, msg, ++this.logId);
      }
      if(this.logConsoleSettingLog) {
        ddb.logs[msg.type](msg.date, msg.fileName, msg.lineNumber, msg.log.padEnd(80, ' '));
      }
    }
    else if(AppProtocolConst.TEST_CASE_STARTED === msg.msgId) {
      if(this.logClientSettingTc) {
        this.name = msg.name;
        this._template(Log.INDEX_START, msg, 1, 1);
      }
    }
    else if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId) {
      if(this.logClientSettingTc) {
        this._template(Log.INDEX_END, msg);
      }
      if(this.logConsoleSettingTc) {
        ddb.print(ActorResultConst.RESULTS_CONSOLE[msg.resultId], this.name);
      }
    }
    else if(AppProtocolConst.TEST_SUITE_STOPPED === msg.msgId) {
      if(this.logConsoleSettingTs) {
        
      }
    }
    else if(AppProtocolConst.TEST_CASE_CLEAR === msg.msgId && 'TestCase' === this.props.clearMsgPrefix) {
      if('log' === msg.tab) {
        this.renderRealtimeData([true, false]);
      }
    }
    else if(AppProtocolConst.TEST_SUITE_CLEAR === msg.msgId && 'TestSuite' === this.props.clearMsgPrefix) {
      if('log' === msg.tab) {
        this.renderRealtimeData([true, false]);
      }
    }
    else if(AppProtocolConst.EXECUTION_STARTED === msg.msgId) {
      this.logClient = !!(1 & msg.chosen);
      this.logConsole = !!(2 & msg.chosen);
      this.logClientSettingLog = this.logClient && !!(1 & msg.clientLogSettings);
      this.logClientSettingTc = this.logClient && !!(2 & msg.clientLogSettings);
      this.logClientSettingTs = this.logClient && !!(4 & msg.clientLogSettings);
      this.logClientSettingSum = this.logClient && !!(8 & msg.clientLogSettings);
      this.logClientSettingErr = this.logClient && !!(16 & msg.clientLogSettings);
      this.logConsoleSettingLog = this.logConsole && !!(1 & msg.clientConsoleLogSettings);
      this.logConsoleSettingTc = this.logConsole && !!(2 & msg.clientConsoleLogSettings);
      this.logConsoleSettingTs = this.logConsole && !!(4 & msg.clientConsoleLogSettings);
      this.logConsoleSettingSum = this.logConsole && !!(8 & msg.clientConsoleLogSettings);
      this.logConsoleSettingErr = this.logConsole && !!(16 & msg.clientConsoleLogSettings);
    }
  }
  
  initComponents(props, update) {
    if(props.buttonsLoaded) {
      let pendings = 0;
      const f = !update ? requestIdleCallback : setTimeout;
      const func = (cb) => {
        ++pendings;
        f(() => {
          //const t0 = performance.now();
          cb();
          //const t1 = performance.now();
          //ddb.debug(`requestIdleCallback took ${t1 - t0} milliseconds.`);
          if(0 === --pendings) {
            props.onOnit();
          }
        });
      };
      this.templates.forEach((template) => {
        func(() => {
          template.init(props.filter);
        });
      });
      func(() => {
        this.scrollList.refresh();
      });
    }
  }
  
  _renderActiveRealtime() {
    if(this.isActive) {
      this.renderRealtimeData();
    }
  }
  
  render() {
    return (
      <>
        <div ref={this.refDivHeading} className="comp_layer_log_heading">
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_nbr">
            #
          </div>
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_type">
            type
          </div>
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_date">
            date
          </div>
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_actor">
            actor
          </div>
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_file_name">
            file name
          </div>
          <div className="comp_layer_log_heading_column comp_layer_log_heading_column_log">
            log
          </div>
        </div>
        <div ref={this.refDivLog} className="comp_layer_log_rows">
        </div>
      </>
    );
  }
}
