
'use strict';

import DataLogLog from '../data/data-log-log';
import ScrollListNode from 'z-abs-complayer-bootstrap-client/client/scroll-list-node';
import LogDataAction from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-action';
import LogPartType from 'z-abs-funclayer-engine-cs/clientServer/log/log-part-type';
import LogType from 'z-abs-funclayer-engine-cs/clientServer/log/log-type';


class TemplateLogLog {
  constructor() {
    this.rowTemplate = null;
    this.filter = null;
    
    this.templateInnerImageRowOpen = null;
    this.templateInnerImageRowClosed = null;
    this.templateInnerImageOpen = null;
    this.templateInnerImageClosed = null;
    
    this.templateInnerWithInnerTextOpen = null;
    this.templateInnerWithInnerTextClosed = null;
    this.templateInnerWithInnerObjectOpen = null;
    this.templateInnerWithInnerObjectClosed = null;
    this.templateInnerWithoutInner = null;
    
    this.templateDynamicInner = null;
    this.templateDynamicInnerWithInner = null;
    this.templateDynamicInnerText = null;
    this.templateDynamicInnerRef = null;
    
    this.templateTestCaseColumns = null;
    this.templateTestCaseInnerRows = null;
    this.templateTestCaseRows = null;
  }
  
  init(filter) {
    this.filter = filter;
    this.templateTestCaseColumns = [[new Array(LogType.names.length), new Array(LogType.names.length)], [new Array(LogType.names.length), new Array(LogType.names.length)]];
    this.templateTestCaseInnerRows = [new Array(LogType.names.length), new Array(LogType.names.length)];
    this.templateTestCaseRows = [[new Array(LogType.names.length), new Array(LogType.names.length)], [new Array(LogType.names.length), new Array(LogType.names.length)]];
    this.templateInnerImageRowOpen = this._createInnerImage(true);
    this.templateInnerImageRowClosed = this._createInnerImage(false);
    this.templateInnerImageOpen = this._createInnerImage(false, 'inner_log_arrow');
    this.templateInnerImageClosed = this._createInnerImage(false, 'inner_log_arrow');
    this.templateInnerWithInnerTextOpen = this._createTemplateInner(true, true, true);
    this.templateInnerWithInnerTextClosed = this._createTemplateInner(true, false, true);
    this.templateInnerWithInnerObjectOpen = this._createTemplateInner(true, true, false);
    this.templateInnerWithInnerObjectClosed = this._createTemplateInner(true, false, false);
    this.templateInnerWithoutInner = this._createTemplateInner(false, false, true);
    this.templateDynamicInner = this._createTemplateDynamicInner(false);
    this.templateDynamicInnerWithInner = this._createTemplateDynamicInner(true);
    this.templateDynamicInnerText = this._createTemplateDynamicInnerText();
    this.templateDynamicInnerRef = this._createTemplateDynamicInnerRef();
    this.templateError = this._createTemplateError();
    this.templateErrorRowRef = this._createTemplateErrorRowRef();
    this.templateErrorRowText = this._createTemplateErrorRowText();
    
    for(let i = 0; i < LogType.names.length; ++i) {
      this.templateTestCaseColumns[TemplateLogLog.OPEN][TemplateLogLog.OUTER][i] = this._createTemplateColumns(i, false, true);
      this.templateTestCaseColumns[TemplateLogLog.CLOSED][TemplateLogLog.OUTER][i] = this._createTemplateColumns(i, false, false);
      this.templateTestCaseColumns[TemplateLogLog.OPEN][TemplateLogLog.INNER][i] = this._createTemplateColumns(i, true, true);
      this.templateTestCaseColumns[TemplateLogLog.CLOSED][TemplateLogLog.INNER][i] = this._createTemplateColumns(i, true, false);
      
      this.templateTestCaseInnerRows[TemplateLogLog.OPEN][i] = this._createTemplateRowInner(i, true);
      this.templateTestCaseInnerRows[TemplateLogLog.CLOSED][i] = this._createTemplateRowInner(i, false);
      
      this.templateTestCaseRows[TemplateLogLog.OPEN][TemplateLogLog.OUTER][i] = this._createTemplateRow(i, TemplateLogLog.OPEN);
      this.templateTestCaseRows[TemplateLogLog.CLOSED][TemplateLogLog.OUTER][i] = this._createTemplateRow(i, TemplateLogLog.CLOSED);
      this.templateTestCaseRows[TemplateLogLog.OPEN][TemplateLogLog.INNER][i] = this._createTemplateRowWithInner(i, TemplateLogLog.OPEN);
      this.templateTestCaseRows[TemplateLogLog.CLOSED][TemplateLogLog.INNER][i] = this._createTemplateRowWithInner(i, TemplateLogLog.CLOSED);
    }
  }
  
  calculateHeight(buffer) {
    const data = DataLogLog.restore(buffer);
    return this.filter.type[data.type] ? 20 : 0;
  }
    
  store(msg, logId) {
    return DataLogLog.store(msg, logId);
  }
  
  restore(buffer) {
    return DataLogLog.restore(buffer);
  }

  create(data) {
    const openId = open ? 0 : 1;
    let node = null;
    //if(!data.inners) {
      node = this.templateTestCaseRows[openId][TemplateLogLog.OUTER][data.type].cloneNode(true);
   // }
   // else {
   //   node = this.templateTestCaseRows[openId][TemplateLogLog.INNER][type].cloneNode(true)
   // }
    node.firstChild.lastChild.nodeValue = data.logId;
    
    if(LogType.IP === data.type) {
      node.childNodes[1].firstChild.textContent += LogDataAction.LOG_SIGN[data.dataId];
    }
    node.childNodes[2].lastChild.nodeValue = data.date;
    node.childNodes[3].firstChild.lastChild.nodeValue = data.actor;
    node.childNodes[3].firstChild.setAttribute('href', data.actorPath);
    node.childNodes[4].firstChild.lastChild.nodeValue = data.fileName;
    node.childNodes[4].firstChild.setAttribute('href', data.codePath);
    node.childNodes[5].lastChild.nodeValue = data.log;
    
    return node;
  }
  
  destroy(node) {}
  
  /*clone(logId, logRow, checked, open, cbResize) {
    const scrollListNodes = [];
    let fragment;
    let node;
    const inners = logRow.inners;
    const openId = open ? 0 : 1;
    if(inners) {
      fragment = this.templateTestCaseRows[openId][TemplateLogLog.INNER][logRow.type].cloneNode(true);
      node = fragment.firstChild;
      const outerNode = new ScrollListNode(node, this, checked ? 20 : 0);
      scrollListNodes.push(outerNode);
      outerNode.addEventListener(`log-filter-type-event-${LogType.pureNames[logRow.type]}`, function(event) {
        this.height = event.detail.checked ? 20 : 0;
      });
      const innerNode = new ScrollListNode(fragment.childNodes[1], this, 0);
      scrollListNodes.push(innerNode);
      let created = false;
      const innerParentNode = fragment.childNodes[1].firstChild;
      const target = node.childNodes[1];
      const arrowTarget = node.childNodes[1].childNodes[1].firstChild;
      const innerTarget = node.nextSibling;
      node.childNodes[1].childNodes[1].addEventListener('click', (evt) => {
       ddb.info('click 1'); 
        innerNode.height = 35;
        cbResize(outerNode.index);
        if(!created) {
          created = true;
          if(!Array.isArray(inners)) {
            this._createInner(inners, innerParentNode, true);
          }
          else {
            inners.forEach((inner) => {
              this._createInner(inner, innerParentNode, true);
            });
          }
        }
        this._toggleEvent(evt, target, arrowTarget, innerTarget);
      }, false);
      if(open) {
        created = true;
        if(!Array.isArray(inners)) {
          this._createInner(inners, innerParentNode, open);
        }
        else {
          inners.forEach((inner) => {
            this._createInner(inner, innerParentNode, open);
          });
        }
      }
    }
    else {
      node = this.templateTestCaseRows[openId][TemplateLogLog.OUTER][logRow.type].cloneNode(true);
      const outerNode = new ScrollListNode(node, this, checked ? 20 : 0);
      scrollListNodes.push(outerNode);
      outerNode.addEventListener(`log-filter-type-event-${LogType.pureNames[logRow.type]}`, function(event) {
        this.height = event.detail.checked ? 20 : 0;
      });
    }
    node.firstChild.lastChild.nodeValue = logId;
    if(LogType.IP === logRow.type) {
      node.childNodes[1].firstChild.textContent += LogDataAction.LOG_SIGN[logRow.data ? logRow.data.id : 0];
    }
    node.childNodes[2].lastChild.nodeValue = `${new Date(logRow.date.date).toUTCString()} : ${logRow.date.milliSeconds}`;
    node.childNodes[3].firstChild.lastChild.nodeValue = logRow.actor;
    node.childNodes[3].firstChild.setAttribute('href', `/actor-editor/${logRow.actorPath}`);
    node.childNodes[4].lastChild.nodeValue = logRow.log;
    node.childNodes[5].firstChild.lastChild.nodeValue = `${logRow.fileName}:${logRow.lineNumber}`;
    if('server' === logRow.sourceType) {
      node.childNodes[5].firstChild.setAttribute('href', `/code-editor/project/${logRow.fileName}?line=${logRow.lineNumber}&type=${logRow.type}`);
    }
    else if('actor' === logRow.sourceType) {
      node.childNodes[5].firstChild.setAttribute('href', `/actor-editor/${logRow.fileName}?line=${logRow.lineNumber}&type=${logRow.type}`);
    }
    else if('stack' === logRow.sourceType) {
      node.childNodes[5].firstChild.setAttribute('href', `/stack-editor/${logRow.fileName}?line=${logRow.lineNumber}&type=${logRow.type}`);
    }
    return scrollListNodes;
  }*/
  
  /*open(nodeData) {
    nodeData.open = true;
    this._resize(nodeData);
    nodeData.cbResize(nodeData.index);
  }
  
  close(nodeData) {
    nodeData.open = false;
    this.resize(nodeData);
    nodeData.cbResize(nodeData.index);
  }
  
  _resize(nodeData) {
    nodeData.height = nodeData.height + 20;
  }*/
  
  _createTemplateRow(logTypeId, openId) {
    const row = document.createElement('div');
    row.classList.add(`comp_layer_${LogType.csses[logTypeId]}`);
    row.classList.add('comp_layer_log_row');
    const columns = this.templateTestCaseColumns[openId][TemplateLogLog.OUTER][logTypeId].cloneNode(true);
    row.appendChild(columns);
    return row;
  }
    
  _createTemplateRowWithInner(logTypeId, openId) {
    const row = document.createElement('div');
    row.classList.add(`comp_layer_${LogType.csses[logTypeId]}`);
    row.classList.add('comp_layer_log_row');
    const columns = this.templateTestCaseColumns[openId][TemplateLogLog.INNER][logTypeId].cloneNode(true);
    row.appendChild(columns);
   // row.appendChild(this.templateTestCaseInnerRows[openId][logTypeId].cloneNode(true));
    return row;
    //const fragment = document.createDocumentFragment();
    //fragment.appendChild(row);
    //fragment.appendChild();
    //return fragment;
  }
  
  _createTemplateColumns(logTypeId, inner, open) {
    const fragment = document.createDocumentFragment();
    const logIdTd = fragment.appendChild(document.createElement('div'));
    logIdTd.classList.add('comp_layer_log_column');
    logIdTd.classList.add('comp_layer_log_column_nbr');
    logIdTd.appendChild(document.createTextNode(''));
    const logTypeTd = fragment.appendChild(document.createElement('div'));
    logTypeTd.classList.add('comp_layer_log_column');
    logTypeTd.classList.add('comp_layer_log_column_type');
    logTypeTd.appendChild(document.createTextNode(LogType.names[logTypeId]));
    if(inner) {
      if(open) {
        logTypeTd.setAttribute('data-inner', 'open');
   	    logTypeTd.appendChild(this.templateInnerImageRowOpen.cloneNode(true));
      }
      else {
        logTypeTd.setAttribute("data-inner", 'closed');
        logTypeTd.appendChild(this.templateInnerImageRowClosed.cloneNode(true));        
      }
    }
    const dateTd = fragment.appendChild(document.createElement('div'));
    dateTd.classList.add('comp_layer_log_column');
    dateTd.classList.add('comp_layer_log_column_date');
    dateTd.appendChild(document.createTextNode(''));
    const actorTd = fragment.appendChild(document.createElement('div'));
    actorTd.classList.add('comp_layer_log_column');
    actorTd.classList.add('comp_layer_log_column_actor');
    const actorA = actorTd.appendChild(document.createElement('a'));
    actorA.classList.add('comp_layer_log_column_actor');
    actorA.appendChild(document.createTextNode(''));
    const fileNameTd = fragment.appendChild(document.createElement('div'));
    fileNameTd.classList.add('comp_layer_log_column');
    fileNameTd.classList.add('comp_layer_log_column_file_name');
    const logTd = fragment.appendChild(document.createElement('div'));
    logTd.classList.add('comp_layer_log_column');
    logTd.classList.add('comp_layer_log_column_log');
    logTd.appendChild(document.createTextNode(''));
    const fileNameA = fileNameTd.appendChild(document.createElement('a'));
    fileNameA.classList.add('comp_layer_log_column_file_name');
    fileNameA.appendChild(document.createTextNode(''));
    return fragment;
  }
  
  _innerClickHandler() {
    
  }
  
  /*_createInner(inner, parent, filterOpen) {
    const inners = inner.inners;
    if(0 !== inners.length) {
      if(inner.open) {
        parent.appendChild(this.templateInnerWithInnerObjectOpen.cloneNode(true));
      }
      else {
        parent.appendChild(this.templateInnerWithInnerObjectClosed.cloneNode(true));
      }
      const div = parent.lastChild.childNodes[1].appendChild(this.templateDynamicInnerWithInner.cloneNode(true));
      inner.logParts.forEach((innerPart) => {
        if(LogPartType.TEXT === innerPart.type) {
          div.appendChild(this.templateDynamicInnerText.cloneNode(true));
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
        else if(LogPartType.REF === innerPart.type) {
          div.appendChild(this.templateDynamicInnerRef.cloneNode(true));
          div.lastChild.setAttribute('href', innerPart.ref);
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
      });
      let created = false;
      const innerParentNode = parent.lastChild.lastChild;
      const target = parent.lastChild;
      const arrowTarget = target.firstChild.firstChild;
      const innerTarget = target.lastChild;
      parent.lastChild.firstChild.addEventListener('click', (evt) => {
        ddb.info('click 2');
        if(!created) {
          created = true;
          inners.forEach((inner) => {
            this._createInner(inner, innerParentNode, filterOpen);
          });
        }
        this._toggleEvent(evt, target, arrowTarget, innerTarget);
      }, false);
      if(filterOpen) {
        created = true;
        inners.forEach((inner) => {
          this._createInner(inner, parent.lastChild.lastChild, filterOpen);
        });
      }
    }
    else {
      const div = parent.appendChild(this.templateDynamicInner.cloneNode(true));
      inner.logParts.forEach((innerPart) => {
        if(LogPartTypeLogPartType.TEXT === innerPart.type) {
          div.appendChild(this.templateDynamicInnerText.cloneNode(true));
          div.lastChild.lastChild.nodeValue = innerPart.text;
        }
        else if(LogPartType.REF === innerPart.type) {
          div.appendChild(this.templateDynamicInnerRef.cloneNode(true));
          div.lastChild.setAttribute('href', innerPart.ref);
          div.lastChild.lastChild.nodeValue = innerPart.text;
        }
        else if(LogPartType.ERROR === innerPart.type) {
          const innerData = innerPart.text;
          const errorDiv = div.appendChild(this.templateError.cloneNode(true));
          errorDiv.firstChild.nodeValue = innerData.error;
          innerData.rows.forEach((row) => {
            if(row.ref) {
              errorDiv.appendChild(this.templateErrorRowRef.cloneNode(true));
              errorDiv.lastChild.firstChild.nodeValue = `  at ${row.func} - `;
              errorDiv.lastChild.lastChild.lastChild.nodeValue = `${row.fileName}:${row.lineNumber}`;
              
              if('server' === row.sourceType) {
                errorDiv.lastChild.lastChild.setAttribute('href', `/code-editor/project/${row.fileName}?line=${row.lineNumber}&type=${LogType.ERROR}`);
              }
              else if('actor' === row.sourceType) {
                errorDiv.lastChild.lastChild.setAttribute('href', `/actor-editor/${row.fileName}?line=${row.lineNumber}&type=${LogType.ERROR}`);
              }
              else if('stack' === row.sourceType) {
                errorDiv.lastChild.lastChild.setAttribute('href', `/stack-editor/${row.fileName}?line=${row.lineNumber}&type=${LogType.ERROR}`);
              }
            }
            else {
              errorDiv.appendChild(this.templateErrorRowRef.cloneNode(true));
              errorDiv.lastChild.firstChild.nodeValue = `  at ${row.func} - ${row.fileName}`;
            }
          });
        }
        else if(LogPartType.BUFFER === innerPart.type) {
          //ddb.error('LogPartType.BUFFER: Not Implemented for log.', innerPart);
        }
      });
    }
  }*/
  
  _toggleEvent(evt, target, arrowTarget, innerTarget/*, cb*/) {
    if('closed' === target.getAttribute('data-inner')) {
      target.setAttribute('data-inner', 'open');
      arrowTarget.setAttribute('transform', 'rotate(45 6 6)');
      innerTarget.classList.remove('inner_closed');
    //  cb && cb(true);
    }
    else {
      target.setAttribute('data-inner', 'closed');
      arrowTarget.removeAttribute('transform');
      innerTarget.classList.add('inner_closed');
   //   cb && cb(false);
    }
  }
  
  _createTemplateRowInner(logTypeId, open) {
    const row = document.createElement('div');
    row.classList.add(LogType.csses[logTypeId]);
    row.classList.add('comp_layer_log_inner');
    if(!open) {
      row.classList.add('inner_closed');
    }
    return row;
  }
  
  _createInnerImage(open, className) {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.classList.add('log_arrow');
    if(undefined !== className) {
      svg.classList.add(className); 
    }
    svg.setAttribute('width', 11);
    svg.setAttribute('height', 11);
    const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    const arrow = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
    arrow.setAttribute('points', '2,1 2,9 10,5');
    arrow.setAttribute('stroke', 'Grey');
    arrow.setAttribute('stroke-width', 1);
    arrow.setAttribute('fill', 'white');
    if(open) {
      group.setAttribute('transform', 'rotate(45 6 6)');
    }
    return svg;
  }
  
  _createTemplateInner(withInner, open, text) {
    const div = document.createElement('div');
    div.classList.add('comp_layer_log_inner');
    if(withInner) {
      if(open) {
        div.setAttribute('data-inner', 'open');
   	    div.appendChild(this.templateInnerImageOpen.cloneNode(true));
        div.firstChild.firstChild.setAttribute('transform', 'rotate(45 6 6)');
      }
      else {
        div.setAttribute('data-inner', 'closed');
   	    div.appendChild(this.templateInnerImageClosed.cloneNode(true));
      }
    }
    if(text) {
      const pre = div.appendChild(document.createElement('pre'));
      pre.classList.add('comp_layer_log_inner');
      pre.appendChild(document.createTextNode(''));
    }
    else {
      const divInner = div.appendChild(document.createElement('div'));
      divInner.classList.add('comp_layer_log_inner');
    }
    if(withInner) {
      const inner = div.appendChild(document.createElement('div'));
      inner.classList.add('comp_layer_log_inner');
      if(!open) {
        inner.classList.add('inner_closed');
      }
    }
    return div;
  }
  
  _createTemplateDynamicInner(withInner) {
    const div = document.createElement('div');
    if(withInner) {
      div.classList.add('inner_dynamic_with_inner');
    }
    else {
      div.classList.add('inner_dynamic');
    }
    return div;
  }

  _createTemplateDynamicInnerText() {
    const pre = document.createElement('pre');
    pre.classList.add('comp_layer_log_inner');
    pre.appendChild(document.createTextNode(''));
    return pre;
  }
  
  _createTemplateDynamicInnerRef() {
    const a = document.createElement('a');
    a.classList.add('comp_layer_log_inner');
    a.setAttribute('href', '[[ref]]');
    a.setAttribute('target', '_blank');
    a.appendChild(document.createTextNode(''));
    return a;
  }
  
  _createTemplateError() {
    const div = document.createElement('div');
    div.appendChild(document.createTextNode(''));
    return div;
  }
  
  _createTemplateErrorRowRef() {
    const pre = document.createElement('pre');
    pre.appendChild(document.createTextNode(''));
    pre.classList.add('comp_layer_log_inner');
    const a = pre.appendChild(document.createElement('a'));
    a.classList.add('comp_layer_log_inner');
    a.setAttribute('href', '[[ref]]');
    a.appendChild(document.createTextNode(''));
    return pre;
  }

  _createTemplateErrorRowText() {
    const pre = document.createElement('pre');
    pre.appendChild(document.createTextNode(''));
    pre.classList.add('comp_layer_log_inner');
    return pre;
  }
}

TemplateLogLog.OUTER = 0;
TemplateLogLog.INNER = 1;
TemplateLogLog.OPEN = 0;
TemplateLogLog.CLOSED = 1;


module.exports = TemplateLogLog;
