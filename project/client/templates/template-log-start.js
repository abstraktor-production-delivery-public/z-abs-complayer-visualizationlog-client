
'use strict';

import DataLogStart from '../data/data-log-start';
import ScrollListNode from 'z-abs-complayer-bootstrap-client/client/scroll-list-node';


class TemplateLogStart {
  constructor() {
    this.rowTemplate = null;
  }
  
  init() {
    this.rowTemplate = document.createElement('div');
    this.rowTemplate.classList.add('comp_layer_log_start_end_row');
    const columnTestCaseId = this.rowTemplate.appendChild(document.createElement('div'));
    columnTestCaseId.classList.add('comp_layer_log_start_end_column_nbr');
    const headingTestCaseId = document.createElement('strong');
    columnTestCaseId.appendChild(headingTestCaseId);
    const columnStart = this.rowTemplate.appendChild(document.createElement('div'));
    columnStart.classList.add('comp_layer_log_start_end_info');
    const headingStart = columnStart.appendChild(document.createElement('strong'));
    headingStart.appendChild(document.createTextNode('TEST CASE START   '));
    const headingName = columnStart.appendChild(document.createElement('strong'));
    headingName.classList.add('log');
    headingName.appendChild(document.createTextNode('name: '));
    const headingDate = columnStart.appendChild(document.createElement('strong'));
    headingDate.classList.add('log');
    headingDate.appendChild(document.createTextNode(' date: '));
  }
  
  calculateHeight(buffer) {
    return 30;
  }
  
  store(msg, testCaseIteration, testSuiteIteration, guid) {
    return DataLogStart.store(msg, testCaseIteration, testSuiteIteration, guid);
  }
  
  restore(buffer) {
    return DataLogStart.restore(buffer);
  }
  
  create(data) {
    const node = this.rowTemplate.cloneNode(true);
    node.id = `tc_log_anchor${data.guid ? '_' + data.guid : ''}_${data.testSuiteIteration}_${data.testCaseId}_${data.testCaseIteration}`
    node.firstChild.firstChild.appendChild(document.createTextNode(data.testCaseId));
    node.childNodes[1].insertBefore(document.createTextNode(data.name), node.childNodes[1].lastChild);
    node.childNodes[1].appendChild(document.createTextNode(data.date));
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateLogStart;
