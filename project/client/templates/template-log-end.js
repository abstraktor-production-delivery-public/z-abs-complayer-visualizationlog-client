
'use strict';

import DataLogEnd from '../data/data-log-end';
import ScrollListNode from 'z-abs-complayer-bootstrap-client/client/scroll-list-node';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';


class TemplateLogEnd {
  constructor() {
    this.rowTemplate = null;
  }
  
  init() {
    this.rowTemplate = new Array(ActorResultConst.resultIds.length);
    for(let resultId = 0; resultId < ActorResultConst.resultIds.length; ++resultId) {
      this.rowTemplate[resultId] = document.createElement('div');
      this.rowTemplate[resultId].classList.add('comp_layer_log_start_end_row');
      const columnTestCaseId = this.rowTemplate[resultId].appendChild(document.createElement('div'));
      columnTestCaseId.classList.add('comp_layer_log_start_end_column_nbr');
      const headingTestCaseId = document.createElement('strong');
      columnTestCaseId.appendChild(headingTestCaseId);
      const columnEnd = this.rowTemplate[resultId].appendChild(document.createElement('div'));
      columnEnd.classList.add('comp_layer_log_start_end_info');
      const heading = columnEnd.appendChild(document.createElement('strong'));
      heading.appendChild(document.createTextNode('TEST CASE END   '));
      const result = columnEnd.appendChild(document.createElement('span'));
      result.appendChild(document.createTextNode(ActorResultConst.getName(resultId)));
      result.classList.add(ActorResultConst.getClass(resultId));
      result.classList.add('log_result');
    }
  }
  
  calculateHeight(buffer) {
    return 30;
  }
  
  store(msg) {
    return DataLogEnd.store(msg);
  }
  
  restore(buffer) {
    return DataLogEnd.restore(buffer);
  }
  
  create(data) {
    const node = this.rowTemplate[data.resultId].cloneNode(true);
    node.firstChild.firstChild.appendChild(document.createTextNode(data.testCaseId));
    node.childNodes[1].appendChild(document.createTextNode(data.duration));
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateLogEnd;
